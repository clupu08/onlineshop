package com.online.shop.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
public class ChosenProduct {

    @Id
    @GeneratedValue
    private Integer chosen_product;

    @ManyToOne
    @JoinColumn
    private ShoppingCart shoppingCart;

    @ManyToOne
    @JoinColumn
    private CustomerOrder customerOrder;

    @Column(name="chosen_quantity")
    private Integer chosenQuantity;

    @ManyToOne
    @JoinColumn
    private Product product;
}
