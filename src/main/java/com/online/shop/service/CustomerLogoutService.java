package com.online.shop.service;

import com.online.shop.Repository.ShoppingCartRepository;
import com.online.shop.entities.ShoppingCart;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Service
public class CustomerLogoutService implements LogoutHandler {
    @Autowired
    private ShoppingCartRepository shoppingCartRepository;

    @Override
    public void logout(HttpServletRequest request, HttpServletResponse response, Authentication authentication) {
        String loggedInUserEmail = authentication.getName();
        ShoppingCart shoppingCart = shoppingCartRepository.findByUserEmail(loggedInUserEmail);
        shoppingCart.setTotal(0);
        shoppingCartRepository.save(shoppingCart);
    }
}
