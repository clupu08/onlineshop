package com.online.shop.service;

import com.online.shop.Repository.*;
import com.online.shop.dto.CustomerOrderDto;
import com.online.shop.entities.*;
import com.online.shop.mapper.CustomerOrderMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CustomerOrderService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ShoppingCartRepository shoppingCartRepository;

    @Autowired
    private ChosenProductRepository chosenProductRepository;

    @Autowired
    private CustomerOrderRepository customerOrderRepository;

    @Autowired
    private ProductRepository productRepository;
@Autowired
    private CustomerOrderMapper customerOrderMapper;

    public void addCustomerOrder(String loggedInUserEmail, String shippingAddress) {
        Optional<User> optionalUser = userRepository.findByEmail(loggedInUserEmail);
        ShoppingCart shoppingCart = shoppingCartRepository.findByUserEmail(loggedInUserEmail);
        User user = optionalUser.get();

        CustomerOrder customerOrder = new CustomerOrder();
        customerOrder.setUser(user);
        if(shippingAddress.isBlank()){
            customerOrder.setShippingAddress(user.getAddress());
        }else{
            customerOrder.setShippingAddress(shippingAddress);
        }

        customerOrderRepository.save(customerOrder);

        for (ChosenProduct chosenProduct : shoppingCart.getChosenProducts()) {

            updateShoppingCart(customerOrder, chosenProduct);

            updateStock(chosenProduct);
        }

        resetTotalNumberOfProducts(loggedInUserEmail);

    }
    private void resetTotalNumberOfProducts(String loggedInUserEmail){
        ShoppingCart shoppingCart = shoppingCartRepository.findByUserEmail(loggedInUserEmail);
        shoppingCart.setTotal(0);
        shoppingCartRepository.save(shoppingCart);
    }

    private void updateStock(ChosenProduct chosenProduct) {
        Integer orderedQuantity = chosenProduct.getChosenQuantity();
        Product product = chosenProduct.getProduct();
        product.setQuantity(product.getQuantity() - orderedQuantity);
        productRepository.save(product);
    }

    private void updateShoppingCart(CustomerOrder customerOrder, ChosenProduct chosenProduct) {
        chosenProduct.setShoppingCart(null);
        chosenProduct.setCustomerOrder(customerOrder);
        chosenProductRepository.save(chosenProduct);
    }

    public List<CustomerOrderDto> getAllCustomerOrderDtos() {
        List<CustomerOrder> allCustomerOrders = customerOrderRepository.findAll();

        List<CustomerOrderDto> customerOrderDtoList = new ArrayList<>();
        for (CustomerOrder customerOrder : allCustomerOrders){
            CustomerOrderDto customerOrderDto = customerOrderMapper.map(customerOrder);
            customerOrderDtoList.add(customerOrderDto);
        }
        return customerOrderDtoList;
    }
}
