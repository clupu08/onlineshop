package com.online.shop.controller;

import com.online.shop.dto.*;
import com.online.shop.service.CustomerOrderService;
import com.online.shop.service.ProductService;
import com.online.shop.service.ShoppingCartService;
import com.online.shop.service.UserService;
import com.online.shop.validator.ChosenProductValidator;
import com.online.shop.validator.ProductValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Controller
public class MainController {
    @Autowired
    private ProductService productService;
    @Autowired
    private ProductValidator productValidator;
    @Autowired
    private ChosenProductValidator chosenProductValidator;

    @Autowired
    private ShoppingCartService shoppingCartService;
    @Autowired
    private UserService userService;

    @Autowired
    private CustomerOrderService customerOrderService;

    @ModelAttribute
    private void addAttributes(Model model) {
        String loggedInUserEmail = SecurityContextHolder.getContext().getAuthentication().getName();
        model.addAttribute("totalNumberOfProducts", shoppingCartService.getTotalNumberOfProducts(loggedInUserEmail));
    }

    @GetMapping("/addProduct")
    public String addProductPageGet(Model model) {
        //se va exista "business logic" :)
        //dupa care intoarcem un nume de pagina

        ProductDto productDto = new ProductDto();
        model.addAttribute("productDto", productDto);
        return "addProduct";
    }

    @PostMapping("/addProduct")
    public String addProductPagePost(@ModelAttribute ProductDto productDto, BindingResult bindingResult,
                                     @RequestParam("productImage") MultipartFile multipartFile) throws IOException {
//        System.out.println(multipartFile.getBytes());
        productValidator.validate(productDto, bindingResult);
        if (bindingResult.hasErrors()) {
            return "addProduct"; //aici am fu flieduri completate de mine
        }
        productService.addProduct(productDto, multipartFile);
        return "redirect:/addProduct"; //aici fara fielduri completate
    }

    @GetMapping("/home")
    public String homepageGet(Model model) {
        List<ProductDto> productDtoList = productService.getAllProductDtos();
        model.addAttribute("productDtoList", productDtoList);
        System.out.println(productDtoList);
        return "homepage";
    }

    @GetMapping("/product/{productId}")
    public String viewProductGet(@PathVariable(value = "productId") String productId, Model model) {

        Optional<ProductDto> optionalProductDto = productService.getProductDtoById(productId);
        if (optionalProductDto.isEmpty()) {
            return "error";
        }
        ProductDto productDto = optionalProductDto.get();
        model.addAttribute("productDto", productDto);

        ChosenProductDto chosenProductDto = new ChosenProductDto();
        model.addAttribute("chosenProductDto", chosenProductDto);
        return "viewProduct";
    }

    @PostMapping("/product/{productId}")
    public ModelAndView viewProductPost(@PathVariable(value = "productId") String productId, ModelAndView model,
                                        @ModelAttribute ChosenProductDto chosenProductDto, BindingResult bindingResult) {

        chosenProductValidator.validate(chosenProductDto, productId, bindingResult);
        String message = null;

        if (bindingResult.hasErrors()) {
            message = Objects.requireNonNull(bindingResult.getFieldError()).getDefaultMessage();
        } else {
            String loggedInUserEmail = SecurityContextHolder.getContext().getAuthentication().getName();
            shoppingCartService.addToCart(chosenProductDto, productId, loggedInUserEmail);
            message = "Product added successfully";
        }
        model.addObject("message", message);
        model.setViewName("messages");
        return model;
    }

        @GetMapping("/contact")
        public String contactPageGet (Model model){
            ProductDto productDto = new ProductDto();
            model.addAttribute("productDto", productDto);

            return "contact";
        }

        @GetMapping("/login")
        public String loginGet () {
            return "login";
        }

        @GetMapping("/cart")
        public String cartGet (Model model){
            String loggedInUserEmail = SecurityContextHolder.getContext().getAuthentication().getName();
            ShoppingCartDto shoppingCartDto = shoppingCartService.getShoppingCartDtoByUserEmail(loggedInUserEmail);
            model.addAttribute("shoppingCartDto", shoppingCartDto);
            return "cart";
        }

        @GetMapping("/checkout")
        public String checkoutGet (Model model){
            String loggedInUserEmail = SecurityContextHolder.getContext().getAuthentication().getName();

            ShoppingCartDto shoppingCartDto = shoppingCartService.getShoppingCartDtoByUserEmail(loggedInUserEmail);
            model.addAttribute("shoppingCartDto", shoppingCartDto);

            UserDetailsDto userDetailsDto = userService.getUserDetailsDtoByEmail(loggedInUserEmail);
            model.addAttribute("userDetailsDto", userDetailsDto);

            return "checkout";
        }
        @PostMapping("/sendOrder")
        public String sendOrder (@ModelAttribute("userDetailsDto") UserDetailsDto userDetailsDto, Model model){
            String loggedInUserEmail = SecurityContextHolder.getContext().getAuthentication().getName();
            customerOrderService.addCustomerOrder(loggedInUserEmail, userDetailsDto.getShippingAddress());

            ShoppingCartDto shoppingCartDto = shoppingCartService.getShoppingCartDtoByUserEmail(loggedInUserEmail);
            model.addAttribute("shoppingCartDto", shoppingCartDto);



            return "confirmation";
        }


        @GetMapping("/ordersTracking")
    public String ordersTrackingPageGet(Model model){
        List<CustomerOrderDto> customerOrderDtoList = customerOrderService.getAllCustomerOrderDtos();
        model.addAttribute("customerOrderDtoList", customerOrderDtoList);

        return "ordersTracking";
        }
    }
